package org.pjsip.pjsua2.app;

import android.text.TextUtils;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.AuthCredInfoVector;

/**
 * Created by adam on 9/25/14.
 */
public class PrankSipProfileFactory {

    public static AccountConfig createPjSipAccConfig (String host, String username, String password, String id){
        AccountConfig config = new AccountConfig();
        updateAccConfig(config, host, username, password, id);
        return config;
    }

    public static void updateAccConfig(AccountConfig config ,String host, String username, String password, String id){
        config.getNatConfig().setIceEnabled(true);
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(host)){
            config.setIdUri(createSipUri(id));
            AuthCredInfoVector creds = config.getSipConfig().getAuthCreds();
            creds.clear();
            if (username.length() != 0) {
                creds.add(new AuthCredInfo("Digest", "*", username, 0, password));
            }
        }
        if (!TextUtils.isEmpty(host)) {
            config.getRegConfig().setRegistrarUri(createSipUri(host));
        }
    }

    public static String createSipUri(String host){
        if (!host.isEmpty() && !host.startsWith("sip:")){
            return "sip:"+host;
        }
        return host;
    }

    public static String createSipURI(String user, String host){
        if (host == null)
            throw new NullPointerException("null host");

        StringBuilder uriString = new StringBuilder("sip:");
        if (user != null) {
            uriString.append(user);
            uriString.append("@");
        }

        //if host is an IPv6 string we should enclose it in sq brackets
        if (host.indexOf(':') != host.lastIndexOf(':')
                && host.trim().charAt(0) != '[')
            host = '[' + host + ']';

        uriString.append(host);
        return uriString.toString();
    }
}
